This is the dependency libs for Android for the CEGUI System - https://bitbucket.org/cegui/cegui

This is support for the following rendering scenarios:

* CEGUI GLES2 renderer using glsles 1.0 shaders
* CEGUI GLES3 renderer using glsles 3.0 shaders (uses GLES2 renderer codebase)
* CEGUI Ogre renderer using Ogre's GLES2 renderer.
* Sample Browser working for all above scenarios


### Instructions for building ###
Download this zip file and unzip.  this will be the location of the CEGUI_DEPENDENCIES_DIR value.
Note that you can us armeabi or armeabi-v7a as the Android ABI value
Download android NDK.  Please use NDK version r9D or below (not the preview 'L' version which is 10/10b)

###ENV and cmake setup###
```
#!c++
export ANDROID_NDK=$HOME/android-ndk-r9d
export ANDROID_SDK=$HOME/android-sdk-linux
PATH=$PATH:$ANDROID_NDK:$ANDROID_SDK/tools:$ANDROID_SDK/platform-tools
cd $HOME/cegui
mkdir cmake_build
cd cmake_build

```

###For building for android 4.3 (API level 18) with GLES3 use the following###
```
#!c++
cmake -DCMAKE_TOOLCHAIN_FILE=../cmake/toolchain/android.toolchain.cmake -DANDROID_NATIVE_API_LEVEL=18 -DANDROID_ABI=armeabi -DCEGUI_BUILD_RENDERER_OGRE=OFF -DCEGUI_DEPENDENCIES_DIR=$HOME/androiddeps  ..
```

###For building for android 2.3 (API level 9) with GLES2 use the following###
```
#!c++
cmake -DCMAKE_TOOLCHAIN_FILE=../cmake/toolchain/android.toolchain.cmake -DANDROID_NATIVE_API_LEVEL=9 -DANDROID_ABI=armeabi -DCEGUI_BUILD_RENDERER_OGRE=OFF -DCEGUI_DEPENDENCIES_DIR=$HOME/androiddeps  ..
```

###For building for android 2.3 (API level 9) with Ogre Renderer using GLES2 use the following###
```
#!c++
cmake -DCMAKE_TOOLCHAIN_FILE=../cmake/toolchain/android.toolchain.cmake -DANDROID_NATIVE_API_LEVEL=9 -DANDROID_ABI=armeabi -DCEGUI_BUILD_RENDERER_OPENGLES2=OFF -DCEGUI_DEPENDENCIES_DIR=$HOME/androiddeps ..
```

###Then just run make (or make install) to build cegui for android.  Then you will have a built android project structure you can examine. ###
```
#!c++
make
cd SampleBrowserNDK
```

###After building the APK can be installed via adb to your android device to run the sample browser demo.  Enjoy!!###
```
#!c++
adb install -r bin/CEGUISampleBrowserAndroid-debug.apk
```